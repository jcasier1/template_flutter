import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:name_of_project/data/src/entity/product/product_entity.dart';
import 'package:name_of_project/data/src/http/http.dart';
import 'package:name_of_project/data/src/repository/product/product_repo.dart';

class Data {
  static Future<void> init() async {
    // Create encryption key if not exist in the secure storage
    const FlutterSecureStorage secureStorage = FlutterSecureStorage();
    final containsEncryptionKey = await secureStorage.containsKey(key: 'encryptionKey');
    if (!containsEncryptionKey) {
      final key = Hive.generateSecureKey();
      await secureStorage.write(key: 'encryptionKey', value: base64UrlEncode(key));
    }

    // Load encryption key
    final encryptionKey = base64Url.decode((await secureStorage.read(key: 'encryptionKey')) ?? '');

    // Init hive layer
    await Hive.initFlutter();

    Hive.registerAdapter(ProductRecordImplAdapter());
    await Hive.openBox<ProductRecord>(productsBoxName, encryptionCipher: HiveAesCipher(encryptionKey));

    // Init http layer
    Http.init();
  }
}
