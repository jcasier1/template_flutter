import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:name_of_project/data/src/converter/string_to_double_converter.dart';
import 'package:uuid/uuid.dart';

part 'product_entity.freezed.dart';
part 'product_entity.g.dart';

@freezed
abstract class ProductEntity with _$ProductEntity {
  ProductEntity._();

  @JsonSerializable(explicitToJson: true, includeIfNull: false)
  factory ProductEntity.dto({
    @JsonKey(name: 'SKU9') @Default('') String sku,
    @JsonKey(name: 'Tarif') @Default(-1) @StringToDouble() double price,
    @JsonKey(name: 'TarifRemise') @Default(-1) @StringToDouble() double discountPrice,
    @JsonKey(name: 'tauxDeRemise') @Default(1) @StringToDouble() double discountRate,
  }) = ProductDto;

  @HiveType(typeId: 0)
  factory ProductEntity.record({
    @HiveField(0) required String id,
    @HiveField(1) @Default('') String sku,
    @HiveField(2) @Default(-1) double price,
    @HiveField(3) @Default(-1) double discountPrice,
    @HiveField(4) @Default(1) double discountRate,
  }) = ProductRecord;

  static ProductRecord toRecord({
    required ProductDto dto,
  }) =>
      ProductRecord(
        id: const Uuid().v4(),
        sku: dto.sku,
        price: dto.price,
        discountPrice: dto.discountPrice,
        discountRate: dto.discountRate,
      );

  factory ProductEntity.fromJson(Map<String, Object?> json) => _$ProductEntityFromJson(json);
}
