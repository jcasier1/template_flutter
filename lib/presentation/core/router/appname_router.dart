import 'package:go_router/go_router.dart';
import 'package:name_of_project/presentation/core/router/appname_route.dart';
import 'package:name_of_project/presentation/feature/login/login_page.dart';
import 'package:name_of_project/presentation/feature/welcome/welcome_page.dart';

final smaRouter = GoRouter(
  routes: <RouteBase>[
    GoRoute(
      path: '/',
      name: kLoginPage,
      builder: (context, state) {
        return LoginPage();
      },
    ),
    GoRoute(
      path: '/welcome',
      name: kWelcomePage,
      builder: (context, state) => const WelcomePage(),
    ),
  ],
);
