import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:name_of_project/domain/src/model/sign/sign_model.dart';

part 'sign_model_notifier.g.dart';

@riverpod
class Sign extends _$Sign {
  @override
  SignModel build() => SignModel();

  set setLogin(String login) {
    state = state.copyWith(login: login);
  }

  set setPassword(String password) {
    state = state.copyWith(password: password);
  }
}
