import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:name_of_project/presentation/core/router/appname_route.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';

class AppNameMenuDrawer extends StatelessWidget {
  const AppNameMenuDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
        child: ListView(
          children: [
            const SizedBox(height: 50),
            Padding(
              padding: const EdgeInsets.only(left: 12),
              child: Text(
                context.i18n.menuTitle,
                style: Theme.of(context).textTheme.headlineLarge,
              ),
            ),
            const SizedBox(height: 10),
            ListTile(
              leading: const Icon(Icons.home),
              title: Text(
                context.i18n.welcomePage,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              onTap: () => context.goNamed(kWelcomePage),
            ),
            ListTile(
              leading: const Icon(Icons.trolley),
              title: Text(
                context.i18n.menuItem2,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              onTap: () {
                // Update the state of the app.
                // ...
              },
            ),
          ],
        ),
      ),
    );
  }
}
