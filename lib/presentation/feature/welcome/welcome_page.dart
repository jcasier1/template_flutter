import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/domain/src/notifier/profile/profile_notifier.dart';
import 'package:name_of_project/presentation/notifier/loading_notifier.dart';
import 'package:name_of_project/presentation/util/extension/context_extension.dart';
import 'package:name_of_project/presentation/widget/functional/card/appname_card.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_scaffold.dart';

class WelcomePage extends HookConsumerWidget {
  const WelcomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final profile = ref.watch(profileNotifierProvider);
    final loading = ref.watch(loadingProvider);

    return AppNameScaffold(
      title: context.i18n.welcomeTitle(profile.name),
      child: Column(
        children: <Widget>[
          Row(
            children: [
              Expanded(
                child: AppNameCard(
                  title: context.i18n.card1,
                  loading: loading,
                  onTap: () => onTapAction1(ref),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: AppNameCard(
                  title: context.i18n.card2,
                  loading: loading,
                  onTap: () => _onTapAction2(ref),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> onTapAction1(WidgetRef ref) async {
    try {
      print('Welcome');
    } finally {
      print('end');
    }
  }

  Future<void> _onTapAction2(WidgetRef ref) async {
    try {
      print('Welcome');
    } finally {
      print('end');
    }
  }
}
