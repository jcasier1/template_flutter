import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class AppNameBackground extends HookWidget {
  const AppNameBackground({
    super.key,
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final controller = useScrollController();

    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Opacity(
          opacity: 0.3,
          child: Image.asset('assets/images/background.png', fit: BoxFit.fitHeight),
        ),
        SingleChildScrollView(
          controller: controller,
          padding: const EdgeInsets.only(top: 20, right: 20, bottom: 0, left: 20),
          child: child,
        ),
      ],
    );
  }
}
