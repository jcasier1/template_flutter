import 'package:flutter/material.dart';
import 'package:name_of_project/presentation/feature/application/drawer/appname_menu_drawer.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/appname_background.dart';

class AppNameScaffold extends StatelessWidget {
  const AppNameScaffold({
    super.key,
    required this.title,
    required this.child,
  });

  final String title;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(title),
      ),
      drawer: const AppNameMenuDrawer(),
      body: AppNameBackground(child: child),
    );
  }
}
