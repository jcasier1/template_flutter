import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/domain/src/model/sign/sign_model.dart';

final signServiceProvider = Provider((ref) => _SignService(ref));

class _SignService {
  _SignService(this.ref);

  final Ref ref;

  bool validate(SignModel model) {
    return model.password == '0000'; // Call api
  }
}
