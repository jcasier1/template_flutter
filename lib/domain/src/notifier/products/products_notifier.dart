import 'package:flutter/material.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:name_of_project/domain/src/model/product/product_model.dart';
import 'package:name_of_project/domain/src/service/product/product_service.dart';
import 'package:name_of_project/presentation/notifier/loading_notifier.dart';

part 'products_notifier.g.dart';

@Riverpod(keepAlive: true)
class Products extends _$Products {
  @override
  List<ProductModel> build() {
    return [];
  }

  Future<void> fetchAll() async {
    try {
      ref.read(loadingProvider.notifier).state = true;
      final models = await ref.read(productServiceProvider).fetchAll();

      state = List.from(models);
    } on Exception catch (e) {
      debugPrint(e.toString());
    } finally {
      ref.read(loadingProvider.notifier).state = false;
    }
  }

  int productsDiscountCount() {
    return state.where((product) => product.discountPrice != '0.0').toList(growable: false).length; // TODO(Anais): To change
  }

  void checkData() {
    if (state.isNotEmpty) return;

    state = ref.read(productServiceProvider).getAll();
  }
}
