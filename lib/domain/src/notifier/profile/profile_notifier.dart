import 'package:flutter/material.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:name_of_project/data/src/dao/profile_dao.dart';
import 'package:name_of_project/domain/src/model/profile/profile_model.dart';

part 'profile_notifier.g.dart';

@Riverpod(keepAlive: true)
class ProfileNotifier extends _$ProfileNotifier {
  @override
  ProfileModel build() => ProfileModel();

  Future<void> get() async {
    try {
      final entity = await ref.read(profileDaoHttpProvider).get();

      state = ProfileModel.fromEntity(entity: entity);
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }
}
